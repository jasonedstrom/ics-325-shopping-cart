<?php
    use data\Item;

    include "data/functions.php";
    require "data/Item.php";

    $output_form = false;

    session_start();

    if ((isset($_SESSION['loggedIn'])) == true) {
        $output_form = true;
        if (isset($_SESSION['cart'])) {
            $cart = $_SESSION['cart'];
        }

        if (isset($_POST['submit'])) {
            $FName    = filter_input(INPUT_POST, 'FName', FILTER_SANITIZE_SPECIAL_CHARS);
            $LName    = filter_input(INPUT_POST, 'LName', FILTER_SANITIZE_SPECIAL_CHARS);
            $Address1 = filter_input(INPUT_POST, 'Address1', FILTER_SANITIZE_SPECIAL_CHARS);
            $Address2 = filter_input(INPUT_POST, 'Address2', FILTER_SANITIZE_SPECIAL_CHARS);
            $City     = filter_input(INPUT_POST, 'City', FILTER_SANITIZE_SPECIAL_CHARS);
            $State    = filter_input(INPUT_POST, 'state', FILTER_SANITIZE_SPECIAL_CHARS);
            $ZipCode  = filter_input(INPUT_POST, 'ZipCode', FILTER_SANITIZE_SPECIAL_CHARS);

            $billing_Info = array(
                "firstName" => $FName,
                "lastName"  => $LName,
                "Address1"  => $Address1,
                "Address2"  => $Address2,
                "City"      => $City,
                "State"     => $State,
                "ZipCode"   => $ZipCode
            );

            $_SESSION['billing_Info'] = $billing_Info;
        }


    }
?>
<!DOCTYPE html>

<head>
    <title>Dev Your Team</title>
    <link rel="stylesheet" id="stylesheet" href="styles/styles.css" type="text/css" media="screen"/>
    <script src="scripts/scripts.js" type="text/javascript"></script>
    <script src=http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js></script>
    <script src="scripts/messi.js" type="text/javascript"></script>
    <link rel="stylesheet" href="styles/messi.css" type="text/css">
</head>

<body>
<div id="header">
    <div id="header_inside">
        <?php
            if (isset($_SESSION['loggedIn'])) {

                if ($_SESSION['loggedIn']) {
                    if (!(isset($_SESSION["FirstName"]) && isset($_SESSION["LastName"]))) {
                        session_destroy();
                    } else {
                        ?>
                        <div id="welcome">
                            Welcome,  <? echo $_SESSION["FirstName"] . " " . $_SESSION["LastName"] . "      "; ?><a
                                href="logout.php" class="buttons">Logout</a>
                            <?php
                                if (isset($_SESSION['administrator'])) {
                                    if ($_SESSION['administrator'] == true) {
                                        ?>
                                        <a href="admin.php" class="buttons">Admin</a>
                                        <a href="cart.php" class="buttons">Cart</a>
                                    <?php
                                    } else {
                                        ?>
                                        <a href="cart.php" class="buttons">Cart</a>
                                    <?php
                                    }
                                }
                            ?>

                        </div>

                    <?php
                    }
                } else {
                    ?>
                    <div><a href="login.php" class="buttons">Login</a> <a href="register.php"
                                                                          class="buttons">Register</a></div>
                <?php
                }
            } else {
                ?>
                <div><a href="login.php" class="buttons">Login</a> <a href="register.php" class="buttons">Register</a>
                </div>
            <?php
            }
        ?>
        <a href="index.php"><h1>Dev <span>Your</span> Team</h1></a>
        <ul>
            <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            <li><a href="developers.php" onclick="Javascript:window.location.assign('developers.php');">Developers</a>
            </li>
            <li><a href="ideas.php" onclick="Javascript:window.location.assign('ideas.php');">Ideas</a></li>
            <li><a href="team.php" onclick="Javascript:window.location.assign('crew.html');">Team</a></li>
            <li><a href="project.php" onclick="Javascript:window.location.assign('projects.html');">Project</a></li>
            <li><a href="index.php" class="active" onclick="Javascript:window.location.assign('index.php');">Home</a>
            </li>
        </ul>
    </div>
</div>
<div id="content">
    <div id="content_inside">
        <div id="content_inside_sidebar">
            <div id="clock">&nbsp;</div>

            <h2>Project</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Team</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Ideas</h2>
            <ul>
                <li><a href="submit.html" onclick="Javascript:window.location.assign('submit.html');">Submit</a></li>
            </ul>
            <h2>Developers</h2>
            <ul>
                <li><a href="wannacode.html" onclick="Javascript:window.location.assign('wannacode.html');">Wanna
                        Code?</a></li>
                <li><a href="apply.html" onclick="Javascript:window.location.assign('apply.html');">Apply Now</a></li>
            </ul>

            <h2>Other</h2>
            <ul>
                <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a>
                </li>
            </ul>
        </div>
        <div id="content_inside_main">

            <h3>Checkout: Payment Page</h3>
            <table class="cart">
                <thead>

                <th>Picture</th>
                <th>Item Name</th>
                <th>Description</th>
                <th class="fixed">Price</th>
                <th>Hours</th>
                <th class="fixed">Item Total</th>
                </thead>
                <tbody>

                <?php
                    if ($output_form){
                    if (!empty($cart)) {
                        $itemNum  = 1;
                        $subtotal = 0;
                        /* @var $cart Item[] */

                        foreach ($cart as $item) {
                            $item_total = (((int)$item->getProductCost()) * ((int)$item->getRequestedHours()));

                            $subtotal += $item_total;
                            $line = "<tr><td><img class='productimg' src='" . $item->getProductImage(
                                ) . "'></td><td>" . $item->getProductName(
                                ) . "</td><td>" . $item->getProductDescription() . "</td><td>$" . $item->getProductCost(
                                ) . " / Hr</td><td>" . $item->getRequestedHours();
                            $line = $line . "</td><td>$$item_total.00</td></tr>";

                            echo $line;
                        }
                    }
                    $service_fee = $subtotal * 0.15;
                    $total = $service_fee + $subtotal;
                ?>
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td>Sub-Total:</td>
                    <td style="font-weight: bold;"><?php echo '$  ' . $subtotal . '.00<input type="hidden" name="subtotal" value="' . $subtotal . '" ' ?> </td>
                </tr>
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td>Service Fee: (15%)</td>
                    <td style="font-weight: bold;"><?php echo '$  ' . $service_fee . '<input type="hidden" name="service_fee" value="' . $service_fee . '" ' ?> </td>
                </tr>
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td>Total:</td>
                    <td style="font-weight: bold;"><?php echo '$  ' . $total . '<input type="hidden" name="cart_total" value="' . $total . '" ' ?> </td>
                </tr>
                </tbody>
            </table>

            <h3>Credit Card Information</h3>

            <form method="post" action='confirm.php'>
                <table class="layout">
                    <tr>
                        <td class="layout"><label for="Card_Type">Card Type: </label></td>
                        <td class="layout"><select name="card_type">
                                <option value="Visa">Visa</option>
                                <option value="Mastercard">Mastercard</option>
                                <option value="American Express">American Express</option>
                                <option value="Discover">Discover</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td class="layout"><label for="cc_number">Credit Card Number: </label></td>
                        <td class="layout"><input type="password" id="cc_number" name="cc_number"
                                                  onChange="fieldCheck(this.id, this.value)" <?php if (isset($_POST['cc_number']) === true) {
                                echo 'value="', strip_tags($_POST['cc_number']), '"';
                            } ?>></td>
                    </tr>
                    <tr>
                        <td class="layout"><label for="Address">Expiration Date: </label></td>
                        <td class="layout"><select name="expiration_month">
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                            <select name="expiration_year">
                                <option value="2014">2014</option>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>

                            </select></td>
                    </tr>
                    <tr>
                        <td class="layout"><label for="ccv2">CCV2: </label></td>
                        <td class="layout"><input type="password" id="ccv2" name="ccv2"
                                                  onChange="fieldCheck(this.id, this.value)" <?php if (isset($_POST['ccv2']) === true) {
                                echo 'value="', strip_tags($_POST['ccv2']), '"';
                            } ?>></td>
                    </tr>
                </table>
                <input type="submit" name='submit' id="submit" value="Next">
            </form>
            <?php

                } else {
                echo "<script>setTimeout(function(){window.location.href = 'login.php';}, 10);</script>";
            }
            ?>
        </div>
    </div>
    <div id="footer">
        <div id="footer_inside">
            <p>Copyright &copy; <a href="#">Dev Your Team</a> 2013 | Designed by <a
                    href="http://www.facebook.com/jasonedstrom" title="Jason Edstrom">Jason Edstrom</a>
        </div>
    </div>
</div>
</body>

</html>