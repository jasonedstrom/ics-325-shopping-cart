<?php

    include "data/functions.php";
    require "data/Item.php";

    $output_form = false;

    session_start();
    if ((isset($_SESSION['loggedIn'])) == true) {
        $output_form = true;
        if ((isset($_GET['user_id'])) || (isset($_SESSION['user_id']))) {

            if (isset($_GET['user_id'])) {
                $user_id = trim(filter_input(INPUT_GET, 'user_id', FILTER_SANITIZE_SPECIAL_CHARS));
            } else {
                if (isset($_SESSION['user_id'])) {
                    $user_id = $_SESSION['user_id'];
                }
            }

            if (isset($_SESSION['administrator']) && $_SESSION['administrator'] == true) {
                $sql = "SELECT * FROM users INNER JOIN ICS325.order ON users.id = order.user_id ORDER BY users.id DESC , order_date DESC ";


                $order_results = SQLquery($sql);
                $order_numRows = $order_results->num_rows;

            } else {
                if (isset($_SESSION['administrator']) && $_SESSION['administrator'] == false) {

                    $sql = "select order_number, order_total, order_date from `order` where user_id=$user_id ORDER BY order_date DESC ";


                    $order_results = SQLquery($sql);
                    $order_numRows = $order_results->num_rows;
                }
            }
            $output_form = true;
        }

    }
?>
<!DOCTYPE html>

<head>
    <title>Dev Your Team</title>
    <link rel="stylesheet" id="stylesheet" href="styles/styles.css" type="text/css" media="screen"/>
    <script src="scripts/scripts.js" type="text/javascript"></script>
    <script src=http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js></script>
    <script src="scripts/messi.js" type="text/javascript"></script>
    <link rel="stylesheet" href="styles/messi.css" type="text/css">
</head>

<body>
<div id="header">
    <div id="header_inside">
        <?php
            if (isset($_SESSION['loggedIn'])) {

                if ($_SESSION['loggedIn']) {
                    if (!(isset($_SESSION["FirstName"]) && isset($_SESSION["LastName"]))) {
                        session_destroy();
                    } else {
                        ?>
                        <div id="welcome">
                            Welcome,  <? echo $_SESSION["FirstName"] . " " . $_SESSION["LastName"] . "      "; ?><a
                                href="logout.php" class="buttons">Logout</a>
                            <?php
                                if (isset($_SESSION['administrator'])) {
                                    if ($_SESSION['administrator'] == true) {
                                        $output_form = true;
                                        ?>
                                        <a href="admin.php" class="buttons">Admin</a>
                                        <a href="cart.php" class="buttons">Cart</a>
                                    <?php
                                    } else {
                                        //$output_form = false;*/
                                        ?>

                                        <a href="cart.php" class="buttons">Cart</a>
                                    <?php
                                    }
                                }
                            ?>

                        </div>

                    <?php
                    }
                } else {
                    ?>
                    <div><a href="login.php" class="buttons">Login</a> <a href="register.php"
                                                                          class="buttons">Register</a></div>
                <?php
                }
            } else {
                ?>
                <div><a href="login.php" class="buttons">Login</a> <a href="register.php" class="buttons">Register</a>
                </div>
            <?php
            }
        ?>
        <a href="index.php"><h1>Dev <span>Your</span> Team</h1></a>
        <ul>
            <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            <li><a href="developers.php" onclick="Javascript:window.location.assign('developers.php');">Developers</a>
            </li>
            <li><a href="ideas.php" onclick="Javascript:window.location.assign('ideas.php');">Ideas</a></li>
            <li><a href="team.php" onclick="Javascript:window.location.assign('crew.html');">Team</a></li>
            <li><a href="project.php" onclick="Javascript:window.location.assign('projects.html');">Project</a></li>
            <li><a href="index.php" class="active" onclick="Javascript:window.location.assign('index.php');">Home</a>
            </li>
        </ul>
    </div>
</div>
<div id="content">
    <div id="content_inside">
        <div id="content_inside_sidebar">
            <div id="clock">&nbsp;</div>

            <h2>Project</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Team</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Ideas</h2>
            <ul>
                <li><a href="submit.html" onclick="Javascript:window.location.assign('submit.html');">Submit</a></li>
            </ul>
            <h2>Developers</h2>
            <ul>
                <li><a href="wannacode.html" onclick="Javascript:window.location.assign('wannacode.html');">Wanna
                        Code?</a></li>
                <li><a href="apply.html" onclick="Javascript:window.location.assign('apply.html');">Apply Now</a></li>
            </ul>

            <h2>Other</h2>
            <ul>
                <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a>
                </li>
            </ul>
        </div>
        <div id="content_inside_main">

            <h3>Order History</h3>

            <table class="cart">
                <thead>
                <?php
                    if (isset($_SESSION['administrator']) && $_SESSION['administrator'] == true) {
                        ?>
                        <th>Order Date</th>
                        <th>Order Number</th>
                        <th>Order Total</th>
                        <th>User</th>
                    <?php
                    } else {
                        if (isset($_SESSION['administrator']) && $_SESSION['administrator'] == false) {

                            ?>
                            <th>Order Date</th>
                            <th>Order Number</th>
                            <th>Order Total</th>

                        <?php
                        }
                    }

                ?>
                </thead>
                <tbody>
                <?php
                    if ($output_form){

                    if ($order_numRows > 0) {
                        while ($row = $order_results->fetch_assoc()) {

                            $order_date = prettyTime($row['order_date']);
                            if (isset($_SESSION['administrator']) && $_SESSION['administrator'] == true) {
                                echo "<tr><td>" . $order_date . "</td><td><a href='view_order.php?order=$row[order_number]'>$row[order_number]</a></td><td>$ $row[order_total]</td><td>$row[FirstName] $row[LastName]</td></tr>";
                            } else {
                                if (isset($_SESSION['administrator']) && $_SESSION['administrator'] == false) {
                                    echo "<tr><td>" . $order_date . "</td><td><a href='view_order.php?order=$row[order_number]'>$row[order_number]</a></td><td>$ $row[order_total]</td></tr>";
                                }
                            }

                        }
                    }

                ?>


                </tbody>
            </table>
            <?php

                } else {
                echo "<script>setTimeout(function(){window.location.href = 'login.php';}, 10);</script>";
            }

            ?>
        </div>
    </div>
    <div id="footer">
        <div id="footer_inside">
            <p>Copyright &copy; <a href="#">Dev Your Team</a> 2013 | Designed by <a
                    href="http://www.facebook.com/jasonedstrom" title="Jason Edstrom">Jason Edstrom</a>
        </div>
    </div>
</div>
</body>

</html>