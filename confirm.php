<?php

    use data\Item;

    include "data/functions.php";
    require "data/Item.php";

    $output_form = false;

    session_start();

    if ((isset($_SESSION['loggedIn'])) == true) {
        $output_form = true;
        if (isset($_SESSION['cart'])) {
            $cart = $_SESSION['cart'];
        }

        if (isset($_POST['submit'])) {
            $billing_Info     = $_SESSION['billing_Info'];
            $card_type        = filter_input(INPUT_POST, 'card_type', FILTER_SANITIZE_SPECIAL_CHARS);
            $cc_number        = filter_input(INPUT_POST, 'cc_number', FILTER_SANITIZE_SPECIAL_CHARS);
            $expiration_month = filter_input(INPUT_POST, 'expiration_month', FILTER_SANITIZE_SPECIAL_CHARS);
            $expiration_year  = filter_input(INPUT_POST, 'expiration_year', FILTER_SANITIZE_SPECIAL_CHARS);
            $ccv2             = filter_input(INPUT_POST, 'ccv2', FILTER_SANITIZE_SPECIAL_CHARS);

            $cc_info = array("card_type"        => $card_type,
                             "cc_number"        => $cc_number,
                             "expiration_month" => $expiration_month,
                             "expiration_year"  => $expiration_year,
                             "ccv2"             => $ccv2
            );

            $_SESSION['cc_info'] = $cc_info;
        }


    }

?>
<!DOCTYPE html>

<head>
    <title>Dev Your Team</title>
    <link rel="stylesheet" id="stylesheet" href="styles/styles.css" type="text/css" media="screen"/>
    <script src="scripts/scripts.js" type="text/javascript"></script>
    <script src=http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js></script>
    <script src="scripts/messi.js" type="text/javascript"></script>
    <link rel="stylesheet" href="styles/messi.css" type="text/css">
</head>

<body>
<div id="header">
    <div id="header_inside">
        <?php
            if (isset($_SESSION['loggedIn'])) {

                if ($_SESSION['loggedIn']) {
                    if (!(isset($_SESSION["FirstName"]) && isset($_SESSION["LastName"]))) {
                        session_destroy();
                    } else {
                        ?>
                        <div id="welcome">
                            Welcome,  <? echo $_SESSION["FirstName"] . " " . $_SESSION["LastName"] . "      "; ?><a
                                href="logout.php" class="buttons">Logout</a>
                            <?php
                                if (isset($_SESSION['administrator'])) {
                                    if ($_SESSION['administrator'] == true) {
                                        ?>
                                        <a href="admin.php" class="buttons">Admin</a>
                                        <a href="cart.php" class="buttons">Cart</a>
                                    <?php
                                    } else {
                                        ?>
                                        <a href="cart.php" class="buttons">Cart</a>
                                    <?php
                                    }
                                }
                            ?>

                        </div>

                    <?php
                    }
                } else {
                    ?>
                    <div><a href="login.php" class="buttons">Login</a> <a href="register.php"
                                                                          class="buttons">Register</a></div>
                <?php
                }
            } else {
                ?>
                <div><a href="login.php" class="buttons">Login</a> <a href="register.php" class="buttons">Register</a>
                </div>
            <?php
            }
        ?>
        <a href="index.php"><h1>Dev <span>Your</span> Team</h1></a>
        <ul>
            <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            <li><a href="developers.php" onclick="Javascript:window.location.assign('developers.php');">Developers</a>
            </li>
            <li><a href="ideas.php" onclick="Javascript:window.location.assign('ideas.php');">Ideas</a></li>
            <li><a href="team.php" onclick="Javascript:window.location.assign('crew.html');">Team</a></li>
            <li><a href="project.php" onclick="Javascript:window.location.assign('projects.html');">Project</a></li>
            <li><a href="index.php" class="active" onclick="Javascript:window.location.assign('index.php');">Home</a>
            </li>
        </ul>
    </div>
</div>
<div id="content">
    <div id="content_inside">
        <div id="content_inside_sidebar">
            <div id="clock">&nbsp;</div>

            <h2>Project</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Team</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Ideas</h2>
            <ul>
                <li><a href="submit.html" onclick="Javascript:window.location.assign('submit.html');">Submit</a></li>
            </ul>
            <h2>Developers</h2>
            <ul>
                <li><a href="wannacode.html" onclick="Javascript:window.location.assign('wannacode.html');">Wanna
                        Code?</a></li>
                <li><a href="apply.html" onclick="Javascript:window.location.assign('apply.html');">Apply Now</a></li>
            </ul>

            <h2>Other</h2>
            <ul>
                <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a>
                </li>
            </ul>
        </div>
        <div id="content_inside_main">
            <h3>Checkout: Confirmation Page</h3>
            <table class="cart">
                <thead>

                <th>Picture</th>
                <th>Item Name</th>
                <th>Description</th>
                <th class="fixed">Price</th>
                <th>Hours</th>
                <th class="fixed">Item Total</th>
                </thead>
                <tbody>

                <?php
                    if ($output_form){
                    if (!empty($cart)) {
                        $itemNum  = 1;
                        $subtotal = 0;
                        /* @var $cart Item[] */

                        foreach ($cart as $item) {
                            $item_total = (((int)$item->getProductCost()) * ((int)$item->getRequestedHours()));

                            $subtotal += $item_total;
                            $line = "<tr><td><img class='productimg' src='" . $item->getProductImage(
                                ) . "'></td><td>" . $item->getProductName(
                                ) . "</td><td>" . $item->getProductDescription() . "</td><td>$" . $item->getProductCost(
                                ) . " / Hr</td><td>" . $item->getRequestedHours();
                            $line = $line . "</td><td>$$item_total.00</td></tr>";

                            echo $line;
                        }
                    }
                    $service_fee = $subtotal * 0.15;
                    $total = $service_fee + $subtotal;
                    $_SESSION['order_total'] = $total;


                ?>
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td>Sub-Total:</td>
                    <td style="font-weight: bold;"><?php echo '$  ' . $subtotal . '.00<input type="hidden" name="subtotal" value="' . $subtotal . '" ' ?> </td>
                </tr>
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td>Service Fee: (15%)</td>
                    <td style="font-weight: bold;"><?php echo '$  ' . $service_fee . '<input type="hidden" name="service_fee" value="' . $service_fee . '" ' ?> </td>
                </tr>
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td>Total:</td>
                    <td style="font-weight: bold;"><?php echo '$  ' . $total . '<input type="hidden" name="cart_total" value="' . $total . '" ' ?> </td>
                </tr>
                </tbody>
            </table>

            <table class="receipt">
                <tr>
                    <td>Name:</td>
                    <td><?php echo $billing_Info['firstName'] . " " . $billing_Info['lastName']; ?></td>
                </tr>
                <tr>
                    <td>Address 1:</td>
                    <td><?php echo $billing_Info['Address1']; ?></td>
                </tr>
                <tr>
                    <td>Address 2:</td>
                    <td><?php echo $billing_Info['Address2']; ?></td>
                </tr>
                <tr>
                    <td>City:</td>
                    <td><?php echo $billing_Info['City']; ?></td>
                </tr>
                <tr>
                    <td>State:</td>
                    <td><?php echo $billing_Info['State']; ?></td>
                </tr>
                <tr>
                    <td>Zip Code:</td>
                    <td><?php echo $billing_Info['ZipCode']; ?></td>
                </tr>
                <tr>
                    <td>Card Number:</td>
                    <td><?php echo censorCC($cc_info['cc_number']); ?></td>
                </tr>
            </table>

            <form id="order_confirm" name="order_confirm" onsubmit="return onsubmitform();" method=post>
                <input type="submit" name="cancel_order" value="Cancel Order"
                       onclick="document.pressed=this.value"><input type="submit" id="checkout" name="submit_order"
                                                                    value="Submit Order"
                                                                    onclick="document.pressed=this.value">
            </form>
            <?php

                } else {
                echo "<script>setTimeout(function(){window.location.href = 'login.php';}, 10);</script>";
            }
            ?>

        </div>
    </div>
    <div id="footer">
        <div id="footer_inside">
            <p>Copyright &copy; <a href="#">Dev Your Team</a> 2013 | Designed by <a
                    href="http://www.facebook.com/jasonedstrom" title="Jason Edstrom">Jason Edstrom</a>
        </div>
    </div>
</div>
</body>

</html>