<?php
    use data\Item;

    include "data/functions.php";
    require "data/Item.php";

    $output_form = false;

    session_start();

    if ((isset($_SESSION['loggedIn'])) == true) {
        $output_order = true;
        if ((isset($_GET['user_id'])) || (isset($_SESSION['user_id']))) {

            if (isset($_GET['user_id'])) {
                $user_id = trim(filter_input(INPUT_GET, 'user_id', FILTER_SANITIZE_SPECIAL_CHARS));
            } else {
                if (isset($_SESSION['user_id'])) {
                    $user_id = $_SESSION['user_id'];
                }
            }
            if (isset($_GET['order'])) {
                $order_number = trim(filter_input(INPUT_GET, 'order', FILTER_SANITIZE_SPECIAL_CHARS));
            }

            $sql = "select * from order_product inner join product on product_id = product.id where order_id =$order_number";
            //find order numbers
            //table links to sql functions to order pull
            //same for all_orders.php

            $product_id_results = SQLquery($sql);
            $product_id_numRows = $product_id_results->num_rows;
            $output_form        = true;


            $order           = array();
            $product_results = SQLquery($sql);
            $product_numRows = $product_results->num_rows;
            if ($product_numRows > 0) {
                while ($product_rows = $product_results->fetch_assoc()) {


                    $item = new \data\Item($product_rows['id'], $product_rows['product_cost'], $product_rows['product_description'], $product_rows['product_hours_available'], "upload/" . $product_rows['product_image'], $product_rows['product_name'], $product_rows['quantity']);

                    array_push($order, $item);
                    //$_SESSION['order'] = $order;
                }

            }
            $order_date;
            $sql           = "select * from `order` where order_number=$order_number";
            $order_results = SQLquery($sql);
            $order_numRows = $order_results->num_rows;
            if ($order_numRows > 0) {
                while ($order_rows = $order_results->fetch_assoc()) {

                    $order_date   = prettyTime($order_rows['order_date']);
                    $cc_number    = $order_rows['cc_number'];
                    $billing_Info = array(
                        "firstName" => $order_rows['first_name'],
                        "lastName"  => $order_rows['last_name'],
                        "Address1"  => $order_rows['address1'],
                        "Address2"  => $order_rows['address2'],
                        "State"     => $order_rows['state'],
                        "City"      => $order_rows['city'],
                        "Zip_Code"  => $order_rows['zip_code']
                    );
                    //$billing_Info = array("firstName"=> $order_rows[])

                }
            }


        }
    }


?>
<!DOCTYPE html>

<head>
    <title>Dev Your Team</title>
    <link rel="stylesheet" id="stylesheet" href="styles/styles.css" type="text/css" media="screen"/>
    <script src="scripts/scripts.js" type="text/javascript"></script>
    <script src=http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js></script>
    <script src="scripts/messi.js" type="text/javascript"></script>
    <link rel="stylesheet" href="styles/messi.css" type="text/css">
</head>

<body>
<div id="header">
    <div id="header_inside">
        <?php
            if (isset($_SESSION['loggedIn'])) {

                if ($_SESSION['loggedIn']) {
                    if (!(isset($_SESSION["FirstName"]) && isset($_SESSION["LastName"]))) {
                        session_destroy();
                    } else {
                        ?>
                        <div id="welcome">
                            Welcome,  <? echo $_SESSION["FirstName"] . " " . $_SESSION["LastName"] . "      "; ?><a
                                href="logout.php" class="buttons">Logout</a>
                            <?php
                                if (isset($_SESSION['administrator'])) {
                                    if ($_SESSION['administrator'] == true) {
                                        $output_form = true;
                                        ?>
                                        <a href="admin.php" class="buttons">Admin</a>
                                        <a href="cart.php" class="buttons">Cart</a>
                                    <?php
                                    } else {
                                        $output_form = false;
                                        ?>

                                        <a href="cart.php" class="buttons">Cart</a>
                                    <?php
                                    }
                                }
                            ?>

                        </div>

                    <?php
                    }
                } else {
                    ?>
                    <div><a href="login.php" class="buttons">Login</a> <a href="register.php"
                                                                          class="buttons">Register</a></div>
                <?php
                }
            } else {
                ?>
                <div><a href="login.php" class="buttons">Login</a> <a href="register.php" class="buttons">Register</a>
                </div>
            <?php
            }
        ?>
        <a href="index.php"><h1>Dev <span>Your</span> Team</h1></a>
        <ul>
            <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            <li><a href="developers.php" onclick="Javascript:window.location.assign('developers.php');">Developers</a>
            </li>
            <li><a href="ideas.php" onclick="Javascript:window.location.assign('ideas.php');">Ideas</a></li>
            <li><a href="team.php" onclick="Javascript:window.location.assign('crew.html');">Team</a></li>
            <li><a href="project.php" onclick="Javascript:window.location.assign('projects.html');">Project</a></li>
            <li><a href="index.php" class="active" onclick="Javascript:window.location.assign('index.php');">Home</a>
            </li>
        </ul>
    </div>
</div>
<div id="content">
    <div id="content_inside">
        <div id="content_inside_sidebar">
            <div id="clock">&nbsp;</div>

            <h2>Project</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Team</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Ideas</h2>
            <ul>
                <li><a href="submit.html" onclick="Javascript:window.location.assign('submit.html');">Submit</a></li>
            </ul>
            <h2>Developers</h2>
            <ul>
                <li><a href="wannacode.html" onclick="Javascript:window.location.assign('wannacode.html');">Wanna
                        Code?</a></li>
                <li><a href="apply.html" onclick="Javascript:window.location.assign('apply.html');">Apply Now</a></li>
            </ul>

            <h2>Other</h2>
            <ul>
                <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a>
                </li>
            </ul>
        </div>
        <div id="content_inside_main">
            <h3>Reciept: Order Number #<?php echo $order_number; ?></h3>
            <h4>Order Date: <?php echo $order_date; ?></h4>
            <table class="cart">
                <thead>

                <th>Picture</th>
                <th>Item Name</th>
                <th>Description</th>
                <th class="fixed">Price</th>
                <th>Hours</th>
                <th class="fixed">Item Total</th>
                </thead>
                <tbody>

                <?php
                    if ($output_form){
                    if (!empty($order)) {

                        $subtotal = 0;
                        /* @var $order Item[] */

                        foreach ($order as $item) {
                            $item_total = (((int)$item->getProductCost()) * ((int)$item->getRequestedHours()));

                            $subtotal += $item_total;
                            $line = "<tr><td><img class='productimg' src='" . $item->getProductImage(
                                ) . "'></td><td>" . $item->getProductName(
                                ) . "</td><td>" . $item->getProductDescription() . "</td><td>$" . $item->getProductCost(
                                ) . " / Hr</td><td>" . $item->getRequestedHours();
                            $line = $line . "</td><td>$$item_total.00</td></tr>";

                            echo $line;
                        }
                    }
                    $service_fee = $subtotal * 0.15;
                    $total = $service_fee + $subtotal;


                ?>
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td>Sub-Total:</td>
                    <td style="font-weight: bold;"><?php echo '$  ' . $subtotal . '.00<input type="hidden" name="subtotal" value="' . $subtotal . '" ' ?> </td>
                </tr>
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td>Service Fee: (15%)</td>
                    <td style="font-weight: bold;"><?php echo '$  ' . $service_fee . '<input type="hidden" name="service_fee" value="' . $service_fee . '" ' ?> </td>
                </tr>
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td>Total:</td>
                    <td style="font-weight: bold;"><?php echo '$  ' . $total . '<input type="hidden" name="order_total" value="' . $total . '" ' ?> </td>
                </tr>
                </tbody>
            </table>

            <table class="receipt">
                <tr>
                    <td>Name:</td>
                    <td><?php echo $billing_Info['firstName'] . " " . $billing_Info['lastName']; ?></td>
                </tr>
                <tr>
                    <td>Address 1:</td>
                    <td><?php echo $billing_Info['Address1']; ?></td>
                </tr>
                <tr>
                    <td>Address 2:</td>
                    <td><?php echo $billing_Info['Address2']; ?></td>
                </tr>
                <tr>
                    <td>City:</td>
                    <td><?php echo $billing_Info['City']; ?></td>
                </tr>
                <tr>
                    <td>State:</td>
                    <td><?php echo $billing_Info['State']; ?></td>
                </tr>
                <tr>
                    <td>Zip Code:</td>
                    <td><?php echo $billing_Info['Zip_Code']; ?></td>
                </tr>
                <tr>
                    <td>Card Number:</td>
                    <td><?php echo censorCC($cc_number); ?></td>
                </tr>
            </table>

            <input type="button" name="orders_page" value="Back to Orders Page" onclick="location.href = 'orders.php'">

            <?php

                } else {
                echo "<script>setTimeout(function(){window.location.href = 'login.php';}, 10);</script>";
            }
            ?>

        </div>
    </div>
    <div id="footer">
        <div id="footer_inside">
            <p>Copyright &copy; <a href="#">Dev Your Team</a> 2013 | Designed by <a
                    href="http://www.facebook.com/jasonedstrom" title="Jason Edstrom">Jason Edstrom</a>
        </div>
    </div>
</div>
</body>

</html>