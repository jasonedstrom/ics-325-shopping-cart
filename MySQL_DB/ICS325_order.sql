CREATE DATABASE  IF NOT EXISTS `ICS325` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ICS325`;
-- MySQL dump 10.13  Distrib 5.6.13, for osx10.6 (i386)
--
-- Host: localhost    Database: ICS325
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.13.10.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `address1` varchar(45) NOT NULL,
  `address2` varchar(45) DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `state` varchar(45) NOT NULL,
  `zip_code` int(11) NOT NULL,
  `card_type` varchar(45) NOT NULL,
  `cc_number` int(11) NOT NULL,
  `ccv2` int(11) NOT NULL,
  `expiration_month` varchar(45) NOT NULL,
  `expiration_year` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `order_total` double NOT NULL,
  `order_date` datetime NOT NULL,
  PRIMARY KEY (`id_order`),
  KEY `id_idx` (`user_id`),
  CONSTRAINT `id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (5,6754146,'Jason','Edstrom','6921 11th Ave South','','Richfield','MN',55423,'Visa',1234567,213456,'January',2014,5,0,'0000-00-00 00:00:00'),(6,7573015,'Jason','Edstrom','6921 11th Ave South','','Richfield','MN',55423,'Visa',123456789,23456789,'January',2014,5,0,'0000-00-00 00:00:00'),(7,7487513,'Jason','Edstrom','6921 11th Ave South','','Richfield','MN',55423,'Visa',2147483647,123,'January',2014,5,0,'0000-00-00 00:00:00'),(8,1954960,'Jason','Edstrom','6921 11th Ave South','','Richfield','MN',55423,'Visa',12345678,1312,'January',2014,5,207,'0000-00-00 00:00:00'),(9,5966859,'Jason','Edstrom','6921 11th Ave South','','Richfield','MN',55423,'Visa',1234,1234,'January',2014,5,276,'0000-00-00 00:00:00'),(10,8964640,'Jason','Edstrom','6921 11th Ave South','','Richfield','MN',55423,'Visa',123456789,123,'January',2014,5,74.75,'0000-00-00 00:00:00'),(11,9042830,'Jason','Edstrom','6921 11th Ave South','','Richfield','MN',55423,'Visa',12345678,1234,'January',2014,5,276,'2014-04-28 11:37:55'),(12,9045924,'Jason','Edstrom','6921 11th Ave South','','Richfield','MN',55423,'Visa',2147483647,5432,'January',2014,6,224.25,'2014-04-28 15:54:58'),(13,7625964,'test','test','test','','test','AZ',55423,'Visa',2135462247,124,'March',2016,6,179.4,'2014-05-05 17:53:16');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-09 21:19:47
