<?php
    /**
     * Created by PhpStorm.
     * User: jasonedstrom
     * Date: 4/26/14
     * Time: 8:19 PM
     */

    namespace data;


    /**
     * Class Item
     * @package data
     */

    class Item
    {
        /**
         * @var
         */
        private $id;
        /**
         * @var
         */
        private $product_Name;
        /**
         * @var
         */
        private $product_Description;
        /**
         * @var
         */
        private $product_Cost;
        /**
         * @var
         */
        private $product_Hours;
        /**
         * @var
         */
        private $product_Image;

        /**
         * @var
         */
        private $requested_Hours;


        /**
         * @param $id
         * @param $product_Cost
         * @param $product_Description
         * @param $product_Hours
         * @param $product_Image
         * @param $product_Name
         * @param $requested_Hours
         */
        public function __construct(
            $id,
            $product_Cost,
            $product_Description,
            $product_Hours,
            $product_Image,
            $product_Name,
            $requested_Hours
        ) {
            $this->id                  = $id;
            $this->product_Cost        = $product_Cost;
            $this->product_Description = $product_Description;
            $this->product_Hours       = $product_Hours;
            $this->product_Image       = $product_Image;
            $this->product_Name        = $product_Name;
            $this->requested_Hours     = $requested_Hours;
        }


        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * @return mixed
         */
        public function getProductCost()
        {
            return $this->product_Cost;
        }

        /**
         * @return mixed
         */
        public function getProductDescription()
        {
            return $this->product_Description;
        }

        /**
         * @return mixed
         */
        public function getProductHours()
        {
            return $this->product_Hours;
        }

        /**
         * @return mixed
         */
        public function getProductImage()
        {
            return $this->product_Image;
        }

        /**
         * @return mixed
         */
        public function getProductName()
        {
            return $this->product_Name;
        }

        /**
         * @param mixed $requested_Hours
         */
        public function setRequestedHours($requested_Hours)
        {
            $this->requested_Hours = $requested_Hours;
        }

        /**
         * @return mixed
         */
        public function getRequestedHours()
        {
            return $this->requested_Hours;
        }


    }