<?php
    use data\Item;

    include "data/functions.php";
    require "data/Item.php";

    $output_cart = false;
    $duplicate = false;
    $cart = array();
    session_start();

    if ((isset($_SESSION['loggedIn'])) == true) {
        $output_cart = true;


        if (isset($_POST['cancel_order'])) {
            unset($_SESSION['cc_info']);
            unset($_SESSION['billing_Info']);
        }


        if ((isset($_POST['remove_item'])) == true) {
            $cart = $_SESSION['cart'];

            for ($i = 1; $i < 50; $i++) {
                if ((isset($_POST['product_remove_' . $i]))) {

                    $id    = $_POST['product_remove_' . $i];
                    $index = 0;
                    foreach ($cart as $item) {

                        if (strcmp($id, $item->getId()) == 0) {
                            unset($cart[$index]);
                        }
                        $index++;
                    }

                }
            }

            if (count($cart) == 0) {
                unset ($_SESSION['cart']);
            } else {
                $_SESSION['cart'] = $cart;
            }
        }


        if ((isset($_POST['update_cart']))) {
            /* @var $cart Item[] */
            $cart = $_SESSION['cart'];
            for ($i = 1; $i < 50; $i++) {
                if ((isset($_POST['product_hours_' . $i]))) {

                    $update_hours = $_POST['product_hours_' . $i];
                    $cart[($i - 1)]->setRequestedHours($update_hours);


                }
            }
            $_SESSION['cart'] = $cart;
        }


        if ((isset($_SESSION['cart'])) && (isset($_GET['id']))) {
            $cart = $_SESSION['cart'];

            $submittedID = trim(filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS));

            /* @var $cart Item[] */
            foreach ($cart as $item) {
                if (strcmp($submittedID, $item->getId()) == 0) {
                    $duplicate = true;
                }
            }

            $output_cart = true;

        } else {
            if (isset($_SESSION['cart'])) {
                $cart = $_SESSION['cart'];


                $output_cart = true;
            }
        }
        if ((isset($_GET['id'])) && !$duplicate) {

            $requested_hours = trim(filter_input(INPUT_GET, 'product_hours', FILTER_SANITIZE_SPECIAL_CHARS));

            $sql             = "Select * from product where id = " . $_GET['id'] . ";";
            $product_results = SQLquery($sql);
            $product_numRows = $product_results->num_rows;
            if ($product_numRows > 0) {
                while ($product_rows = $product_results->fetch_assoc()) {


                    $item = new \data\Item($product_rows['id'], $product_rows['product_cost'], $product_rows['product_description'], $product_rows['product_hours_available'], "upload/" . $product_rows['product_image'], $product_rows['product_name'], $requested_hours);
                    //$item = array(['id']->$product_rows['id'], $$product_rows['product_cost'], $product_rows['product_description'], $product_rows['product_hours_available'], "upload/".$product_rows['product_image'], $product_rows['product_name']);
                    array_push($cart, $item);
                    $_SESSION['cart'] = $cart;
                }

            }
            $output_cart = true;
        }
    } else {
        $output_cart = false;
    }


?>
<!DOCTYPE html>

<head>
    <title>Dev Your Team</title>
    <link rel="stylesheet" id="stylesheet" href="styles/styles.css" type="text/css" media="screen"/>
    <script src="scripts/scripts.js" type="text/javascript"></script>
    <script src=http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js></script>
    <!--    <script src="scripts/messi.js" type="text/javascript"></script>-->
    <link rel="stylesheet" href="styles/messi.css" type="text/css">
</head>

<body>
<div id="header">
    <div id="header_inside">
        <?php
            if (isset($_SESSION['loggedIn'])) {

                if ($_SESSION['loggedIn']) {
                    if (!(isset($_SESSION["FirstName"]) && isset($_SESSION["LastName"]))) {
                        session_destroy();
                    } else {
                        ?>
                        <div id="welcome">
                            Welcome,  <? echo $_SESSION["FirstName"] . " " . $_SESSION["LastName"] . "      "; ?><a
                                href="logout.php" class="buttons">Logout</a>
                            <?php
                                if (isset($_SESSION['administrator'])) {
                                    if ($_SESSION['administrator'] == true) {
                                        ?>
                                        <a href="admin.php" class="buttons">Admin</a>
                                        <a href="cart.php" class="buttons">Cart</a>
                                    <?php
                                    } else {
                                        ?>
                                        <a href="cart.php" class="buttons">Cart</a>
                                    <?php
                                    }
                                }
                            ?>

                        </div>

                    <?php
                    }
                } else {
                    ?>
                    <div><a href="login.php" class="buttons">Login</a> <a href="register.php"
                                                                          class="buttons">Register</a></div>
                <?php
                }
            } else {
                ?>
                <div><a href="login.php" class="buttons">Login</a> <a href="register.php" class="buttons">Register</a>
                </div>
            <?php
            }
        ?>
        <a href="index.php"><h1>Dev <span>Your</span> Team</h1></a>
        <ul>
            <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            <li><a href="developers.php" onclick="Javascript:window.location.assign('developers.php');">Developers</a>
            </li>
            <li><a href="ideas.php" onclick="Javascript:window.location.assign('ideas.php');">Ideas</a></li>
            <li><a href="team.php" onclick="Javascript:window.location.assign('crew.html');">Team</a></li>
            <li><a href="project.php" onclick="Javascript:window.location.assign('projects.html');">Project</a></li>
            <li><a href="index.php" class="active" onclick="Javascript:window.location.assign('index.php');">Home</a>
            </li>
        </ul>
    </div>
</div>
<div id="content">
    <div id="content_inside">
        <div id="content_inside_sidebar">
            <div id="clock">&nbsp;</div>

            <h2>Project</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Team</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Ideas</h2>
            <ul>
                <li><a href="submit.html" onclick="Javascript:window.location.assign('submit.html');">Submit</a></li>
            </ul>
            <h2>Developers</h2>
            <ul>
                <li><a href="wannacode.html" onclick="Javascript:window.location.assign('wannacode.html');">Wanna
                        Code?</a></li>
                <li><a href="apply.html" onclick="Javascript:window.location.assign('apply.html');">Apply Now</a></li>
            </ul>

            <h2>Other</h2>
            <ul>
                <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a>
                </li>
            </ul>
        </div>
        <div id="content_inside_main">

            <?php
                if ($output_cart) {

                    if ($duplicate) {
                        echo " <ul> <li>Duplicate items can not be added to cart.</li></ul>";
                    }

                    ?>

                    <h3>Shopping Cart</h3>
                    <form name='cart' id='cart' method='post' onsubmit="return onsubmitform();">
                        <table class="cart">
                            <thead>
                            <!--                            <th>Picture</th><th>Item Name</th><th>Description</th><th>Project Hours</th><th>Price</th>-->
                            <th>Remove</th>
                            <th>Picture</th>
                            <th>Item Name</th>
                            <th>Description</th>
                            <th class="fixed">Price</th>
                            <th>Hours</th>
                            <th class="fixed">Item Total</th>
                            </thead>
                            <tbody>

                            <?php
                                if (!empty($cart)){
                                $itemNum = 1;
                                $total = 0;
                                /* @var $cart Item[] */

                                foreach ($cart as $item) {
                                    $item_total = (((int)$item->getProductCost()) * ((int)$item->getRequestedHours()));
                                    $total += $item_total;
                                    $line = "<tr><td><input type='checkbox' name='product_remove_" . $itemNum . "' value='" . $item->getId(
                                        ) . "'> </td><td><img class='productimg' src='" . $item->getProductImage(
                                        ) . "'></td><td><a href='product.php?id=" . $item->getId(
                                        ) . "'>" . $item->getProductName(
                                        ) . "</a></td><td>" . $item->getProductDescription(
                                        ) . "</td><td>$" . $item->getProductCost(
                                        ) . " / Hr<input type='hidden' name='product_cost_" . $itemNum . "' value='" . $item->getProductCost(
                                        ) . "'></td><td><input type='number' min='1' max='" . $item->getProductHours(
                                        ) . "' name='product_hours_" . $itemNum . "' value='" . $item->getRequestedHours(
                                        ) . "'";
                                    $line = $line . "></td><td>$$item_total.00</td></tr>";

                                    echo $line;
                                }
                            ?>
                            <tr>
                                <td style="border: none;"></td>
                                <td style="border: none;"></td>
                                <td style="border: none;"></td>
                                <td style="border: none;"></td>
                                <td style="border: none;"></td>
                                <td>Total:</td>
                                <td style="font-weight: bold;"><?php echo '$  ' . $total . '.00<input type="hidden" name="cart_total" value="' . $total . '" ' ?> </td>
                            </tr>
                            </tbody>
                        </table>
                        <?php
                            } else {
                            echo "</table><div style='text-align: center;'>No Items In Shopping Cart</div>";
                        }
                        ?>
                        <input type='submit' name='remove_item' value='Remove Item'
                               onclick="document.pressed=this.value"><input type='submit' name='update_cart'
                                                                            value='Update Cart'
                                                                            onclick="document.pressed=this.value"><input
                            type=submit name='checkout' id='checkout' value='Checkout'
                            onclick="document.pressed=this.value">
                    </form>


                <?php

                } else {
                    echo "<script>setTimeout(function(){window.location.href = 'login.php';}, 10);</script>";
                }
            ?>

        </div>
    </div>
    <div id="footer">
        <div id="footer_inside">
            <p>Copyright &copy; <a href="#">Dev Your Team</a> 2013 | Designed by <a
                    href="http://www.facebook.com/jasonedstrom" title="Jason Edstrom">Jason Edstrom</a>
        </div>
    </div>
</div>
</body>

</html>