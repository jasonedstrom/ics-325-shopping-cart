<?php

    use data\Item;

    include "data/functions.php";
    require "data/Item.php";

    $output_form = false;

    session_start();

    if ((isset($_SESSION['loggedIn'])) == true) {
        $output_form = true;
        if (isset($_SESSION['cart'])) {
            $cart = $_SESSION['cart'];

            if ((isset($_POST['checkout']))) {
                /* @var $cart Item[] */
                $cart = $_SESSION['cart'];
                for ($i = 1; $i < 50; $i++) {
                    if ((isset($_POST['product_hours_' . $i]))) {

                        $update_hours = $_POST['product_hours_' . $i];
                        $cart[($i - 1)]->setRequestedHours($update_hours);


                    }
                }
                $_SESSION['cart'] = $cart;
            }
        }
    }

?>
<!DOCTYPE html>

<head>
    <title>Dev Your Team</title>
    <link rel="stylesheet" id="stylesheet" href="styles/styles.css" type="text/css" media="screen"/>
    <script src="scripts/scripts.js" type="text/javascript"></script>
    <script src=http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js></script>
    <script src="scripts/messi.js" type="text/javascript"></script>
    <link rel="stylesheet" href="styles/messi.css" type="text/css">
</head>

<body>
<div id="header">
    <div id="header_inside">
        <?php
            if (isset($_SESSION['loggedIn'])) {

                if ($_SESSION['loggedIn']) {
                    if (!(isset($_SESSION["FirstName"]) && isset($_SESSION["LastName"]))) {
                        session_destroy();
                    } else {
                        ?>
                        <div id="welcome">
                            Welcome,  <? echo $_SESSION["FirstName"] . " " . $_SESSION["LastName"] . "      "; ?><a
                                href="logout.php" class="buttons">Logout</a>
                            <?php
                                if (isset($_SESSION['administrator'])) {
                                    if ($_SESSION['administrator'] == true) {
                                        ?>
                                        <a href="admin.php" class="buttons">Admin</a>
                                        <a href="cart.php" class="buttons">Cart</a>
                                    <?php
                                    } else {
                                        ?>
                                        <a href="cart.php" class="buttons">Cart</a>
                                    <?php
                                    }
                                }
                            ?>

                        </div>

                    <?php
                    }
                } else {
                    ?>
                    <div><a href="login.php" class="buttons">Login</a> <a href="register.php"
                                                                          class="buttons">Register</a></div>
                <?php
                }
            } else {
                ?>
                <div><a href="login.php" class="buttons">Login</a> <a href="register.php" class="buttons">Register</a>
                </div>
            <?php
            }
        ?>
        <a href="index.php"><h1>Dev <span>Your</span> Team</h1></a>
        <ul>
            <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a></li>
            <li><a href="developers.php" onclick="Javascript:window.location.assign('developers.php');">Developers</a>
            </li>
            <li><a href="ideas.php" onclick="Javascript:window.location.assign('ideas.php');">Ideas</a></li>
            <li><a href="team.php" onclick="Javascript:window.location.assign('crew.html');">Team</a></li>
            <li><a href="project.php" onclick="Javascript:window.location.assign('projects.html');">Project</a></li>
            <li><a href="index.php" class="active" onclick="Javascript:window.location.assign('index.php');">Home</a>
            </li>
        </ul>
    </div>
</div>
<div id="content">
    <div id="content_inside">
        <div id="content_inside_sidebar">
            <div id="clock">&nbsp;</div>

            <h2>Project</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Team</h2>
            <ul>
                <li><a href="" onclick="Javascript:window.location.assign('');">HTML/CSS</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Javascript/Ajax</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">PHP</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">J2EE</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Database</a></li>
                <li><a href="" onclick="Javascript:window.location.assign('');">Mobile</a></li>
            </ul>
            <h2>Ideas</h2>
            <ul>
                <li><a href="submit.html" onclick="Javascript:window.location.assign('submit.html');">Submit</a></li>
            </ul>
            <h2>Developers</h2>
            <ul>
                <li><a href="wannacode.html" onclick="Javascript:window.location.assign('wannacode.html');">Wanna
                        Code?</a></li>
                <li><a href="apply.html" onclick="Javascript:window.location.assign('apply.html');">Apply Now</a></li>
            </ul>

            <h2>Other</h2>
            <ul>
                <li><a href="contact.php" onclick="Javascript:window.location.assign('contact.php');">Contact Us</a>
                </li>
            </ul>
        </div>
        <div id="content_inside_main">
            <h3>Order Overview Page : Checkout</h3>
            <table class="cart">
                <thead>

                <th>Picture</th>
                <th>Item Name</th>
                <th>Description</th>
                <th class="fixed">Price</th>
                <th>Hours</th>
                <th class="fixed">Item Total</th>
                </thead>
                <tbody>

                <?php
                    if ($output_form){
                    if (!empty($cart)) {
                        $itemNum  = 1;
                        $subtotal = 0;
                        /* @var $cart Item[] */

                        foreach ($cart as $item) {
                            $item_total = (((int)$item->getProductCost()) * ((int)$item->getRequestedHours()));

                            $subtotal += $item_total;
                            $line = "<tr><td><img class='productimg' src='" . $item->getProductImage(
                                ) . "'></td><td>" . $item->getProductName(
                                ) . "</td><td>" . $item->getProductDescription() . "</td><td>$" . $item->getProductCost(
                                ) . " / Hr</td><td>" . $item->getRequestedHours();
                            $line = $line . "</td><td>$$item_total.00</td></tr>";

                            echo $line;
                        }
                    }
                    $service_fee = $subtotal * 0.15;
                    $total = $service_fee + $subtotal;
                ?>
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td>Sub-Total:</td>
                    <td style="font-weight: bold;"><?php echo '$  ' . $subtotal . '.00<input type="hidden" name="subtotal" value="' . $subtotal . '" ' ?> </td>
                </tr>
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td>Service Fee: (15%)</td>
                    <td style="font-weight: bold;"><?php echo '$  ' . $service_fee . '<input type="hidden" name="service_fee" value="' . $service_fee . '" ' ?> </td>
                </tr>
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td>Total:</td>
                    <td style="font-weight: bold;"><?php echo '$  ' . $total . '<input type="hidden" name="cart_total" value="' . $total . '" ' ?> </td>
                </tr>
                </tbody>
            </table>
            <h3>Billing Information</h3>

            <form method="post" action='payment.php'>
                <table class="layout">
                    <tr>
                        <td class="layout"><label for="FName">First Name: </label></td>
                        <td class="layout"><input type="text" id="FName" name="FName"
                                                  onChange="fieldCheck(this.id, this.value)" <?php if (isset($_POST['FName']) === true) {
                                echo 'value="', strip_tags($_POST['FName']), '"';
                            } ?>></td>
                    </tr>
                    <tr>
                        <td class="layout"><label for="LName">Last Name: </label></td>
                        <td class="layout"><input type="text" id="LName" name="LName"
                                                  onChange="fieldCheck(this.id, this.value)" <?php if (isset($_POST['LName']) === true) {
                                echo 'value="', strip_tags($_POST['LName']), '"';
                            } ?>></td>
                    </tr>
                    <tr>
                        <td class="layout"><label for="Address">Address 1: </label></td>
                        <td class="layout"><input type="text" id="Address1" name="Address1"
                                                  onChange="fieldCheck(this.id, this.value)" <?php if (isset($_POST['Address1']) === true) {
                                echo 'value="', strip_tags($_POST['Address1']), '"';
                            } ?>></td>
                    </tr>
                    <tr>
                        <td class="layout"><label for="Address2">Address 2: </label></td>
                        <td class="layout"><input type="text" id="Address2" name="Address2"
                                                  onChange="fieldCheck(this.id, this.value)" <?php if (isset($_POST['Address2']) === true) {
                                echo 'value="', strip_tags($_POST['Address2']), '"';
                            } ?>></td>
                    </tr>
                    <tr>
                        <td class="layout"><label for="City">City: </label></td>
                        <td class="layout"><input type="text" id="City" name="City"
                                                  onChange="fieldCheck(this.id, this.value)"<?php if (isset($_POST['City']) === true) {
                                echo 'value="', strip_tags($_POST['City']), '"';
                            } ?>></td>
                    </tr>
                    <tr>
                        <td class="layout"><label for="State">State: </label></td>
                        <td class="layout">
                            <select name="state" id="stateSelect" onChange="fieldCheck(this.id, this.value)" size="1">
                                <option value="Choose">Choose</option>
                                <optgroup label="USA">
                                    <option value="AL">Alabama (AL)</option>
                                    <option value="AK">Alaska (AK)</option>
                                    <option value="AZ">Arizona (AZ)</option>
                                    <option value="AR">Arkansas (AR)</option>
                                    <option value="CA">California (CA)</option>
                                    <option value="CO">Colorado (CO)</option>
                                    <option value="CT">Connecticut (CT)</option>
                                    <option value="DE">Delaware (DE)</option>
                                    <option value="FL">Florida (FL)</option>
                                    <option value="GA">Georgia (GA)</option>
                                    <option value="HI">Hawaii (HI)</option>
                                    <option value="ID">Idaho (ID)</option>
                                    <option value="IL">Illinois (IL)</option>
                                    <option value="IN">Indiana (IN)</option>
                                    <option value="IA">Iowa (IA)</option>
                                    <option value="KS">Kansas (KS)</option>
                                    <option value="KY">Kentucky (KY)</option>
                                    <option value="LA">Louisiana (LA)</option>
                                    <option value="ME">Maine (ME)</option>
                                    <option value="MD">Maryland (MD)</option>
                                    <option value="MA">Massachusetts (MA)</option>
                                    <option value="MI">Michigan (MI)</option>
                                    <option value="MN">Minnesota (MN)</option>
                                    <option value="MS">Mississippi (MS)</option>
                                    <option value="MO">Missouri (MO)</option>
                                    <option value="MT">Montana (MT)</option>
                                    <option value="NE">Nebraska (NE)</option>
                                    <option value="NV">Nevada (NV)</option>
                                    <option value="NH">New Hampshire (NH)</option>
                                    <option value="NJ">New Jersey (NJ)</option>
                                    <option value="NM">New Mexico (NM)</option>
                                    <option value="NY">New York (NY)</option>
                                    <option value="NC">North Carolina (NC)</option>
                                    <option value="ND">North Dakota (ND)</option>
                                    <option value="OH">Ohio (OH)</option>
                                    <option value="OK">Oklahoma (OK)</option>
                                    <option value="OR">Oregon (OR)</option>
                                    <option value="PA">Pennsylvania (PA)</option>
                                    <option value="RI">Rhode Island (RI)</option>
                                    <option value="SC">South Carolina (SC)</option>
                                    <option value="SD">South Dakota (SD)</option>
                                    <option value="TN">Tennessee (TN)</option>
                                    <option value="TX">Texas (TX)</option>
                                    <option value="UT">Utah (UT)</option>
                                    <option value="VT">Vermont (VT)</option>
                                    <option value="VA">Virginia (VA)</option>
                                    <option value="WA">Washington (WA)</option>
                                    <option value="WV">West Virginia (WV)</option>
                                    <option value="WI">Wisconsin (WI)</option>
                                    <option value="WY">Wyoming (WY)</option>
                                </optgroup>
                                <optgroup label="Canada" class="country">
                                    <option value="AB">Alberta (AB)</option>
                                    <option value="BC">British Columbia (BC)</option>
                                    <option value="MB">Manitoba (MB)</option>
                                    <option value="NB">New Brunswick (NB)</option>
                                    <option value="NL">New Foundland (NL)</option>
                                    <option value="NS">Nova Scotia (NS)</option>
                                    <option value="ON">Ontario (ON)</option>
                                    <option value="PE">Prince Edward Island (PE)</option>
                                    <option value="QC">Quebec (QC)</option>
                                    <option value="SL">Saskatchewan (SL)</option>
                                </optgroup>
                            </select></td>
                    </tr>


                    <tr>
                        <td class="layout"><label for="ZipCode">Zip Code: </label></td>
                        <td class="layout"><input type="text" id="ZipCode" name="ZipCode"
                                                  onChange="fieldCheck(this.id, this.value)"></td>
                    </tr>


                </table>

                <input type="submit" name='submit' id="submit" value="Next">
            </form>
            <?php

                } else {
                echo "<script>setTimeout(function(){window.location.href = 'login.php';}, 10);</script>";
            }
            ?>

        </div>
    </div>
    <div id="footer">
        <div id="footer_inside">
            <p>Copyright &copy; <a href="#">Dev Your Team</a> 2013 | Designed by <a
                    href="http://www.facebook.com/jasonedstrom" title="Jason Edstrom">Jason Edstrom</a>
        </div>
    </div>
</div>
</body>

</html>